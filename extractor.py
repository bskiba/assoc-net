# -*- coding: utf-8 -*-

# from __future__ import unicode_literals
import json

from processor import *
from corpora.wrapper import CorpusWrapper
from corpora.pap.provider import PapProvider, RestrictedPapProvider
from utils.tokenizer import Tokenizer
from utils.normalizer import Normalizer 
from assocnet.proxy import AssocNet
from utils.presentation import show_graph_with_dot
from utils.presentation import save_graph_with_neato, save_graph_with_spring
from utils.parser import get_base_form


import networkx as nx

import codecs
import datetime
import os
import shutil


DEFAULT_CONFIG = {
	"ignore_adjectives": True,
	"graph_size_limit": True,
}

ADJECTIVES_CONFIG = {
	"ignore_adjectives": False,
	"graph_size_limit": True,
}

NO_LIMIT_CONFIG = {
	"ignore_adjectives": True,
	"graph_size_limit": False,
}

class Extractor:
	def __init__(self, corpus, config, network):
		self.config = config
		self.corpus = corpus
		self.ignore_adjectives = self.config["ignore_adjectives"]
		self.graph_size_limit = self.config["graph_size_limit"]
		self.network = network

	def extract_subgraph_for_note(self, note, word):
		return find_best_subgraph(note,
								  word,
								  self.network.network,
								  ignore_adjectives=self.ignore_adjectives,
								  graph_size_limit=self.graph_size_limit)

	def get_notes_with_word(self, word):
		notes_with_word = []
		for i in xrange(len(self.corpus.get_tokenized_texts())):
			tokenized_note = self.corpus.get_tokenized_texts()[i]
			normalized_note = self.corpus.get_normalized_texts()[i]
			if self.word_in_note(word, tokenized_note, normalized_note):
				notes_with_word.append((i, normalized_note))
		return notes_with_word

	def word_in_note(self, word, tokenized_note, normalized_note):
		# TODO: think about base forms - where to use them and where not. 
		# This should work for now, but could be changed in the future
		return word in tokenized_note or get_base_form(word) in normalized_note
	
	def get_results_for_word(self, word):
		notes_with_word = self.get_notes_with_word(word)
		results = []
		for (i, note) in notes_with_word:
			result = self.extract_subgraph_for_note(note, word)
			# Result will be None if there wheren't enough associations in the text.
			if result:
				(subgraph, extracting_nodes) = result
				results.append(Result(i, self.corpus.get_texts()[i], extracting_nodes, subgraph))
		return results

	def get_results_for_words(self, words):
		results_for_words = {}
		for word in words:
			results_for_words[word] = self.get_results_for_word(word)
		return results_for_words


	def get_texts(self):
		return self.corpus.get_texts()

def relabel_to_unicode(graph):
	# for node in graph:
	# 	print isinstance(node, unicode)
	nx.relabel_nodes(graph, mapping = dict(zip(graph, map(unicode, graph))), copy = False)

class Result():
	def __init__(self, note_id, note, extracting_nodes, subgraph):
		self.note_id = note_id
		self.note = note
		self.extracting_nodes = extracting_nodes
		self.subgraph = subgraph

#TODO think about better name
#TODO move this somewhere else, don't know where yet
class Persister():

	RESULTS_DIR_FORMAT = os.path.join(
			"results",
			"results_{0}_1")

	GRAPHS_PER_WORD_DIR = "graphs_per_word"
	GRAPHS_PER_NOTE_DIR = "graphs_per_note"

	CONFIG_FILENAME = "results.cfg"

	def __init__(self, results_for_words, config):
		self.results_for_words = results_for_words
		self.config = config
		date = datetime.date.today()
		#TODO if exists add _1
		self.results_dir = self.get_results_dirname(date)
		self.graphs_per_word_dir = self.get_graphs_per_word_dirname(self.results_dir)
		self.graphs_per_note_dir = self.get_graphs_per_note_dirname(self.results_dir)
		self.make_all_dirs()

	def make_all_dirs(self):
		os.makedirs(self.results_dir)
		os.makedirs(self.graphs_per_word_dir)
		os.makedirs(self.graphs_per_note_dir)

	def get_results_dirname(self, date):
		return Persister.RESULTS_DIR_FORMAT.format(date.isoformat())

	def get_graphs_per_word_dirname(self, results_dir):
		return os.path.join(results_dir, Persister.GRAPHS_PER_WORD_DIR)

	def get_graphs_per_note_dirname(self, results_dir):
		return os.path.join(results_dir, Persister.GRAPHS_PER_NOTE_DIR)

	def persist_results(self):
		for (word, results) in self.results_for_words.iteritems():
			if len(results) > 0:
				self.persist_results_for_word(word, results)

		self.persist_config()

	def persist_config(self):
		config_filename = self.get_config_filename()
		with open(config_filename, 'w') as config_file:
			json.dump(self.config, config_file)

	def get_config_filename(self):
		return os.path.join(self.results_dir, Persister.CONFIG_FILENAME)

	def persist_results_for_word(self, word, results):
		word_dir = self.create_word_dir(word)
		#TODO refactor inside of the loop
		for result in results:
			note_dir = self.create_note_dir_if_necessary(result.note_id)
			
			word_note_filename = os.path.join(word_dir, "{0}_note.txt".format(result.note_id))
			word_extracting_nodes_filename = os.path.join(word_dir, "{0}_extracting_nodes.txt".format(result.note_id))
			word_graph_filename = os.path.join(word_dir, "{0}_graph.png".format(result.note_id))

			note_note_filename = os.path.join(note_dir, "note.txt")
			note_extracting_nodes_filename = os.path.join(note_dir, u"{0}_extracting_nodes.txt".format(word))
			note_graph_filename = os.path.join(note_dir, u"{0}_graph.png".format(word))

			# Todo: move this to result - create persist functions that take filename as parameter
			with codecs.open(word_note_filename, encoding='utf-8', mode='w') as note_file:
				note_file.write(result.note)
			with codecs.open(word_extracting_nodes_filename, encoding='utf-8', mode='w') as extracting_nodes_file:
				extracting_nodes_file.write(", ".join(result.extracting_nodes))
			save_graph_with_neato(result.subgraph, word_graph_filename)

			#copy stuff for note
			if not os.path.exists(note_note_filename):
				shutil.copyfile(word_note_filename, note_note_filename)
			shutil.copyfile(word_extracting_nodes_filename, note_extracting_nodes_filename)
			shutil.copyfile(word_graph_filename, note_graph_filename)


	def create_word_dir(self, word):
		word_dir = self.get_dirname_for_word(word)
		os.makedirs(word_dir)
		return word_dir

	def create_note_dir_if_necessary(self, note_id):
		note_dir = self.get_dirname_for_note(note_id)
		if not os.path.exists(note_dir):
			os.makedirs(note_dir)
		return note_dir

	def get_dirname_for_word(self, word):
		return os.path.join(self.graphs_per_word_dir, word)

	def get_dirname_for_note(self, note_id):
		return os.path.join(self.graphs_per_note_dir, str(note_id))

if __name__ == '__main__':
	pap = CorpusWrapper(RestrictedPapProvider(), Tokenizer(), Normalizer())
	network = AssocNet()
	config = DEFAULT_CONFIG
	pap_extractor = Extractor(pap, config, network)
	print "Built extractor"
	# print pap_extractor.get_subgraphs_for_word(u'dom')[0]
	# basic_stimuli = network.get_basic_stimuli()
	basic_stimuli = [u"prawa", u"praca", u"sąd", u"wieku", u"prawo"]
	# basic_stimuli = [u"prawa"]
	results_for_words = pap_extractor.get_results_for_words(basic_stimuli)
	print "Built subgraphs"
	persister = Persister(results_for_words, config)
	persister.persist_results()
	print "Finished persisting"
	