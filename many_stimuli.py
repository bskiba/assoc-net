from extractor import Extractor
from corpora.wrapper import CorpusWrapper
from corpora.pap.provider import PapProvider
from assocnet.proxy import AssocNet
from utils.tokenizer import Tokenizer
from utils.normalizer import Normalizer 

def get_notes_with_many_stimuli(stimuli, extractor, threshold = 10):
	stimuli_number_per_note = {}
	for word in stimuli:
		notes_with_word = extractor.get_notes_with_word(word)
		for (i, note) in notes_with_word:
			try:
				stimuli_number_per_note[i] += 1
			except:
				stimuli_number_per_note[i] = 1

	return [note for note in stimuli_number_per_note if stimuli_number_per_note[note] >= threshold] 


	
if __name__ == '__main__': 
	pap = CorpusWrapper(PapProvider(), Tokenizer(), Normalizer())
	network = AssocNet()
	pap_extractor = Extractor(pap, network)
	basic_stimuli = network.get_basic_stimuli()
	print "Built extractor"
	notes = get_notes_with_many_stimuli(basic_stimuli, pap_extractor)
	print notes