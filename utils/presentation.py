# -*- coding: utf-8 -*-

import networkx as nx
import matplotlib

matplotlib.use('GTKCairo')

import pylab

pylab.rc('font', family='Arial')

# pylab.rc('text', usetex=True)
# pylab.rc('text.latex',unicode=True)
# pylab.rc('text.latex',preamble='\usepackage[T1]{polski}')

def show_node_with_neighbours(network, word):
	if word not in network:
		raise ValueError("Word %s not present in the network" % (word,))
	subgraph = network.subgraph([word] + network.neighbors(word))

	pylab.figure(1)

	pos=nx.graphviz_layout(subgraph, prog = 'twopi', root = word)
	nx.draw(subgraph,pos,node_size = 2000, with_labels = True, node_color = "white", )
	edge_labels = {(u, v) : d["weight"] for u, v, d in subgraph.edges_iter(data = True)}
	nx.draw_networkx_edge_labels(subgraph,pos, edge_labels)
	pylab.show()


def show_graph_with_dot(graph):
	show_graph_with_prog(graph, 'dot')

def show_graph_with_neato(graph):
	show_graph_with_prog(graph, 'neato')

def save_graph_with_dot(graph, filename):
	save_graph_with_prog(graph, filename, 'dot')

def save_graph_with_neato(graph, filename):
	save_graph_with_prog(graph, filename, 'neato')

def save_graph_with_prog(graph, filename, prog):
	get_graph_with_prog(graph, prog)
	pylab.savefig(filename)
	pylab.close()

def save_graph_with_spring(graph, filename):
	pylab.figure(1)
	pos=nx.spring_layout(graph)
	nx.draw(graph,pos,node_size = 2000, with_labels = True, node_color = "white")
	edge_labels = {(u, v) : "%.2e" % d["weight"] for u, v, d in graph.edges_iter(data = True)}
	nx.draw_networkx_edge_labels(graph,pos, edge_labels)
	pylab.savefig(filename)
	pylab.close()

def show_graph_with_prog(graph, prog):
	get_graph_with_prog(graph, prog)
	pylab.show()
	pylab.close()



def get_graph_with_prog(graph, prog):
	pylab.figure(1)
	pos=nx.graphviz_layout(graph, prog = prog)
	nx.draw(graph,pos,node_size = 2000, with_labels = True, node_color = "white")
	edge_labels = {(u, v) : "%.2e" % d["weight"] for u, v, d in graph.edges_iter(data = True)}
	nx.draw_networkx_edge_labels(graph,pos, edge_labels)
