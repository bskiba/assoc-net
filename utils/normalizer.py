from plp import PLP

#TODO not sure this is the best it can be
# I don't know yet how this is going to work with graphs that need this too
class Normalizer():
	def __init__(self):
		self.plp = PLP()

	def normalize_word(self, word):
		word_base_form = self.get_base_form(word)
		return word_base_form

	def normalize_text(self, text):
		return [self.normalize_word(word) for word in text]

	def get_base_form(self, word):
		forms = self.plp.rec(word)
		# Leave words that are not present in dictionary unchanged.
		if len(forms) == 0:
			return word
		return self.plp.bform(forms[0])
