import re

from utils.tokenizer import Tokenizer

from plp import PLP

p = PLP()

#TODO: refactor and think about better tokenization
#TODO: maybe saved parsed corpus to file, but not necessarily

class Parser:
	def __init__(self, corpus):
		self.corpus = corpus
		self.tokenizer = Tokenizer()
		self.tokenized_texts = None
		self.normalized_texts = None

	def get_tokenized_texts(self):
		if not self.tokenized_texts:
			self.tokenized_texts = []
			for text in self.corpus:
				words = self.tokenizer.tokenize(text)
				self.tokenized_texts.append(words)
		return self.tokenized_texts

	def get_normalized_texts(self):
		if not self.normalized_texts:
			self.normalized_texts = []
			tokenized_texts = self.get_tokenized_texts
			for text in tokenized_texts:
				normalized_text = set()
				for word in text:
					normalized_text.add()
				self.normalized_texts

def get_base_form(word):
	forms = p.rec(word)
	# Leave words that are not in dictionary unchanged.
	if len(forms) == 0:
		return word
	return p.bform(forms[0])

def get_part_of_speech(word):
	ids = p.rec(word)
	if len(ids) == 0:
		return None
	word_id = ids[0]
	label = p.label(word_id)
	return label[0]

def is_adjective(word):
	return get_part_of_speech(word) == p.CZESCI_MOWY.PRZYMIOTNIK