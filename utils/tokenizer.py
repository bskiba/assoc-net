# -*- coding: utf-8 -*-

import re

class Tokenizer:
	def __init__(self):
		self.regex = re.compile(r'\w+\b', re.UNICODE) 
	
	def tokenize(self, text):
		# I am not happy that this should be here, it's not part of tokenization, but it is 
		# needed to help  find stimuli in texts
		words = self.regex.findall(text)
		return [word.lower() for word in words]

if __name__ == "__main__":
	t = Tokenizer()
	print t.tokenize(u"Łapa trala lala.")