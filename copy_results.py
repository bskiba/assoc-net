import os
import datetime
import re
import shutil

date = datetime.date.today()
results_dir = os.path.join(
		"results",
		"results_{0}".format(date.isoformat()))
graphs_per_word_dir = os.path.join(results_dir, "graphs_per_word")
graphs_per_note_dir = os.path.join(results_dir, "graphs_per_note")

regex = re.compile("([0-9]*)_(.*)")

i = 0
word_dirs = {}
for root, dirs, files in os.walk(graphs_per_word_dir):
	# print root, dirs , files
	if i == 0:
		words = dirs
	else:
		word = words[i - 1] 
		for filename in files:
			result = regex.match(filename)
			print result.groups()
			note_i = result.groups()[0]
			note_dir = os.path.join(graphs_per_note_dir, note_i)
			newfilename = os.path.join(note_dir, "{0}_{1}".format(word, result.groups()[1]))
			if not os.path.exists(note_dir):
				os.makedirs(note_dir)
			shutil.copyfile(os.path.join(root,filename),newfilename)
	i += 1