import networkx as nx
import cPickle as pickle
import os

def load_unweighted_network():
	network = nx.read_gpickle("assocnet/network.pickle")
	return network

def load_weighted_network():
	network = nx.read_gpickle("assocnet/network_weights.pickle")
	return network

def load_network():
	return load_weighted_network()

NETWORK_PATH = "/home/beata/mgr/siec"

def load_basic_stimuli():
	basic_stimuli = []
	for root, dirs, files in os.walk(NETWORK_PATH):
		for filename in files:
			#remove file ending
			stimulus = filename[:-4]
			stimulus = stimulus.lower()
			stimulus = unicode(stimulus, 'utf-8')
			basic_stimuli.append(stimulus)
	return basic_stimuli