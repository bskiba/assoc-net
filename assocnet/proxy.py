from assocnet.loader import * 

# Not sure if this is necessary, we're working well without it using loader
class AssocNet():
	def __init__(self, weighted = True):
		self.weighted = weighted
		self.network = None
		if(weighted):
			self.network = load_weighted_network()
		else:
			self.network = load_unweighted_network()
		self.basic_stimuli = load_basic_stimuli()

	def get_network(self):
		return self.network

	def get_basic_stimuli(self):
		return self.basic_stimuli

#TODO: forward all other method calls to network
# def __getattr__(self, name):
# """Forward methods to board. AttributeError will be raised fo unknown methods. """
# attr = getattr(self.board, name)
# return attr