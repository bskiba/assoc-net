#!/usr/bin/env python
from __future__ import print_function

import os
from os.path import join
import csv

import networkx as nx
import pylab

import cPickle as pickle

import assocnet.loader


NETWORK_PATH = "/home/beata/mgr/siec"

root = NETWORK_PATH

def add_edges_from_file(filename, root, network):
	word =filename[:-4]
	full_name = join(root, filename)
	print(full_name)
	if not full_name.endswith('~'):
		with open(full_name) as src:
			filereader = csv.reader(src, delimiter = ";")
			for row in filereader:
				assoc = unicode(row[0], 'utf-8')
				weight = int(row[1])
				network.add_edge(unicode(word, 'utf-8'), assoc, weight = weight)


def build_unweighted_network(verbose = False):
	network = nx.DiGraph()

	for filename in os.listdir(root):
	    add_edges_from_file(filename, root, network)

	nx.write_gpickle(network, "network.pickle")

	# present_some_data(network,'baranina')

	if verbose:
		print("Built network stats:")
		print("edges: %d" % network.number_of_edges())
		print("nodes: %d" % network.number_of_nodes())

def build_weighted_network(verbose = False):
	try:
		unweighted_network = loader.load_unweighted_network()
	except IOError:
		if verbose:
			"No unweighted netwok found, building unweighted network."
		build_unweighted_network(verbose = verbose)
		unweighted_network = loader.load_unweighted_network()

	if verbose:
		print("Building weighted network.")

	network = nx.DiGraph()

	for node in unweighted_network:
		total_weight = 0
		for source, target, data in unweighted_network.edges(node, data = True):
			weight = data["weight"]
			total_weight += weight
		for source, target, data in unweighted_network.edges(node, data = True):
			weight = data["weight"]
			new_weight =  total_weight * 1. / weight 
			network.add_edge(source, target, weight = new_weight)

	nx.write_gpickle(network, "network_weights.pickle")
	if verbose:
		print("Built network stats:")
		print("edges: %d" % network.number_of_edges())
		print("nodes: %d" % network.number_of_nodes())

if __name__ == '__main__':
	build_weighted_network(verbose = True)

