import networkx as nx

from loader import load_unweighted_network

network = load_unweighted_network()

new_network = nx.DiGraph()

for node in network:
	total_weight = 0
	for source, target, data in network.edges(node, data = True):
		weight = data["weight"]
		total_weight += weight
	for source, target, data in network.edges(node, data = True):
		weight = data["weight"]
		new_weight =  total_weight * 1. / weight 
		new_network.add_edge(source.lower(), target.lower(), weight = new_weight)

nx.write_gpickle(new_network, "assocnet/network_weights.pickle")