#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

from utils.presentation import show_node_with_neighbours, show_graph_with_dot, show_graph_with_neato
from assocnet.loader import load_network, load_unweighted_network

def test_netwrok():
	network = load_network()

	for source, target, data in network.edges(u'baranina', data = True):
		print(source, target, data["weight"])
	show_node_with_neighbours(network, u'baranina')


def dom_example():
	network = load_network()

	data = {
		u"rodzinny" : 125,
		u"mieszkanie" : 113,
		u"rodzina" : 82,
		u"spokój" : 21,
		u"ciepło " : 19,
		u"ogród" : 18,
		u"mój" : 17,
		u"bezpieczeństwo" : 13,
		u"dach" : 12,
		u"pokój" : 11,
		u"mama" : 11
	}

	network.remove_nodes_from(network.nodes())

	network.add_node(u"dom")
	total = 0
	for assoc, num in data.items():
		total += num

	for assoc, num in data.items():
		network.add_edge(u"dom", assoc, {"weight" : total * 1./ num})

	show_node_with_neighbours(network, u"dom")

def get_nodes_pointing_at(network, node):
	ret = []
	for (n1, n2) in network.edges():
		if n2 == node:
			ret.append(n1)
	return ret

def primary_node_example():
	word = u"dom"
	network = load_network()
	pointing_at = get_nodes_pointing_at(network, word)
	sorted_pointing_at = sorted(pointing_at, key = lambda n : network[n][word]["weight"])
	sorted_neighbours = sorted(network.neighbors(word), key = lambda n : network[word][n]["weight"])
	nodes = set([word] + sorted_pointing_at[:3] + sorted_neighbours[:5])
	subgraph = network.subgraph(nodes)
	to_remove = []
	for edge in subgraph.edges():
		if word not in edge:
			to_remove.append(edge)
	subgraph.remove_edges_from(to_remove)
	show_graph_with_neato(subgraph)

def secondary_node_example():
	word = u"spokój"
	network = load_network()
	pointing_at = get_nodes_pointing_at(network, word)
	sorted_pointing_at = sorted(pointing_at, key = lambda n : network[n][word]["weight"])
	nodes = set([word] + sorted_pointing_at[:7])
	subgraph = network.subgraph(nodes)
	to_remove = []
	for edge in subgraph.edges():
		if word not in edge:
			to_remove.append(edge)
	subgraph.remove_edges_from(to_remove)
	show_graph_with_neato(subgraph)

if __name__ == '__main__':
	secondary_node_example()