import re

PAP_LOCATION = "/home/beata/mgr/korpus/pap-all.not"

class PapProvider():
	"""Abstracts real pap corpus location in file system and the process of 
	breaking it up into seperate notes."""

	def __init__(self, location = PAP_LOCATION):
		self.location = location
		# Detects ids preceding each of the notes in the file 
		# that separate texts
		self.note_regex = re.compile(r'#[0-9]+', re.UNICODE)
		self.texts = self.parse_corpus()

	def parse_corpus(self):
		with open(self.location, 'r') as pap_file:
			corpus_text = unicode(pap_file.read(), 'iso-8859-2')
			texts = self.parse_corpus_text(corpus_text)
		return texts

	def parse_corpus_text(self, corpus_text):
		texts = re.split(self.note_regex, corpus_text)
		# First element is empty, there is no text before first note id
		return texts[1:]

NOTES_WITH_MANY_STIMULI = [847, 2781, 2874, 3091, 5148, 5325, 7974, 9087, 10119, 10441, 10712, 12614, 12709, 12817, 13144, 15561, 17825, 18259, 19156, 21328, 23204, 23221, 
	24377, 25221, 25995, 27573, 28040, 29302, 29540, 29638, 33677, 33806, 36640, 36949, 36951, 36952, 37082, 37387, 37706, 38728, 38831, 39979, 41973, 46451, 49094, 49676]

class RestrictedPapProvider():
	"""Restricts the corpus to selected notes"""
	def __init__(self, location = PAP_LOCATION, note_ids = NOTES_WITH_MANY_STIMULI):
		self.pap_provider = PapProvider(location)
		self.texts = [self.pap_provider.texts[note_id] for note_id in note_ids]