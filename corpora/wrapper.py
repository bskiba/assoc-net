class CorpusWrapper():
	"""Wraps a corpus (collection of texts) given by a provider
	by making it possible to get original and processed texts. """
	
	def __init__(self, provider, tokenizer, normalizer):
		self.provider = provider
		self.tokenizer = tokenizer
		self.normalizer = normalizer
		self.texts = None
		self.tokenized_texts = None
		self.normalized_texts = None

	def get_texts(self):
		return self.provider.texts

	def get_tokenized_texts(self):
		if not self.tokenized_texts:
			self.tokenized_texts = self.tokenize_texts(self.get_texts())
		return self.tokenized_texts

	# TODO: maybe move method to tokenizer
	def tokenize_texts(self, texts):
		return [self.tokenizer.tokenize(text) for text in texts]

	def get_normalized_texts(self):
		if not self.normalized_texts:
			tokenized_texts = self.get_tokenized_texts()	
			self.normalized_texts = self.normalize_texts(tokenized_texts)
		return self.normalized_texts

	# TODO: maybe move method to normalizer
	def normalize_texts(self, texts):
		return [self.normalizer.normalize_text(text) for text in texts]