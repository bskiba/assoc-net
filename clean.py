#!/usr/bin/env python
from __future__ import print_function

import os
from os.path import join
import csv

from plp import PLP


NETWORK_PATH = "/home/beata/mgr/siec_skojarzeniowa/csv"
DEST_PATH = "/home/beata/mgr/siec"
p = PLP()

def process_file(filename, root):

	full_name = join(root,filename)
	dest_name = join(DEST_PATH, filename)

	with open(full_name, 'r') as src:
		with open(dest_name, 'w') as dest:
			# omit the file ending
			word = filename[:-4]
			filereader = csv.reader(src, delimiter = ';')
			filewriter = csv.writer(dest, delimiter = ';')

			print('____________________')
			print('associations for ' + word)

			for row in filereader:
				assoc = unicode(row[0], 'utf-8')
				if  len(p.rec(assoc)) == 0:
					print(assoc)
					approval = raw_input()
					if approval != 'n':
						filewriter.writerow(row)
					else:
						print('omitting ' + assoc)
				else:
					filewriter.writerow(row)

for root, dirs, files in os.walk(NETWORK_PATH):
    for filename in files:
    	process_file(filename, root)