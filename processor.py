# -*- coding: utf-8 -*-

import networkx as nx
import itertools
import random

from utils.presentation import show_graph_with_dot
from assocnet.loader import load_network
from utils.parser import get_base_form, is_adjective

MAX_INTERMEDIATE_VERTICES = 3

#TODO: save all the graphs found, with all the info about text, associations used, etc.
#TODO: work on line length
def find_best_subgraph(text, stimulus, network, max_intermediate_vertices = MAX_INTERMEDIATE_VERTICES, 
		ignore_adjectives = True, graph_size_limit = True):
	associations = network.neighbors(stimulus)
	# TODO: this is not optimal. It does not even work, if the word is not in network, exception is thrown
	# Need to think through how to best approach the base froms issue
	# Maybe we can just assume that the stimulus is in base form. But still that leaves us with the problem
	# that the stimulus might not be in its base form in the graph or it might have multiple forms in the graph 
	# witch one to choose in that case? 
	if not associations:
		associations = network.neighbors(get_base_form(stimulus))
	if ignore_adjectives:
		associations_in_text = [assoc for assoc in associations if get_base_form(assoc) in text and assoc != stimulus
					and not is_adjective(assoc)]
	else:
		associations_in_text = [assoc for assoc in associations if get_base_form(assoc) in text and assoc != stimulus]

	# Base forms of associations. The dictionary allows to reconstruct the original association.
	base_associations_in_text = {assoc:get_base_form(assoc) for assoc in associations_in_text}

	if len(base_associations_in_text) < 2:
		return None
	# Get basic extracted subgraph for 2 associations. 
	extracting_nodes = [stimulus]
	extracting_nodes = extracting_nodes + random.sample(base_associations_in_text, 2)
	previous_extracting_nodes = extracting_nodes[:]
	
	basic_extracted_subgraph = extract_subgraph(extracting_nodes, network, max_intermediate_vertices)
	previous_extracted_subgraph = basic_extracted_subgraph
	extracted_subgraph = basic_extracted_subgraph

	for associations_number in xrange(3, len(base_associations_in_text)):

		previous_extracting_nodes = extracting_nodes[:]
		previous_extracted_subgraph = extracted_subgraph.copy()

		extracting_nodes.append(
			random.choice(
				[assoc for assoc in base_associations_in_text.keys() 
					if assoc not in extracting_nodes]
			)
		)

		extracted_subgraph = extract_subgraph(extracting_nodes, network, max_intermediate_vertices)
		
		if graph_size_limit:
			if extracted_subgraph_too_big(basic_extracted_subgraph, extracted_subgraph):
				extracted_subgraph = previous_extracted_subgraph
				extracting_nodes = previous_extracting_nodes
				break
	return (extracted_subgraph, extracting_nodes)

# Right now this looks at all premutations of extracting nodes, creates all possible results on every stage
# and just marks those, that are deemed too big.
# TODO: Careful, we have multiple instances of same subgraph. Maybe change this to all different permutations
# also of different lengths and not save graph stages 
# Returns a dictionary, where we associate each permutation with a list of stages procured by using this permutation 
def find_all_best_subgraphs(text, stimulus, network, max_intermediate_vertices = MAX_INTERMEDIATE_VERTICES, 
		ignore_adjectives = True, graph_size_limit = True):

	all_permutations = get_all_extracting_nodes_permutations(text, stimulus, network, ignore_adjectives)

	all_results = {}

	for all_extracting_nodes in all_permutations:

		results_per_permutation = [] 

		# Get basic extracted subgraph for 2 associations. 
		extracting_nodes = all_extracting_nodes[:3]

		basic_extracted_subgraph = extract_subgraph(extracting_nodes, network, max_intermediate_vertices)
		extracted_subgraph = basic_extracted_subgraph

		for extracting_nodes_number in xrange(4, len(all_extracting_nodes)):

			extracting_nodes = all_extracting_nodes[:extracting_nodes_number]

			extracted_subgraph = extract_subgraph(extracting_nodes, network, max_intermediate_vertices)
			
			if graph_size_limit:
				if extracted_subgraph_too_big(basic_extracted_subgraph, extracted_subgraph):
					is_too_big = True

			stage = GraphStage(extracting_nodes, extracted_subgraph, is_too_big)
			results_per_permutation.append(stage)
		all_results[all_extracting_nodes] = results_per_permutation

	return all_results

def extracted_subgraph_too_big(basic_extracted_subgraph, extracted_subgraph):
	if len(basic_extracted_subgraph) < len(extracted_subgraph) / 2:
		return False
	return True

def extract_subgraph(extracting_nodes, network, max_intermediate_vertices = MAX_INTERMEDIATE_VERTICES):
    """
    Given current extracted graph, runs an iteration of extracting algorithm and 
    returns the new extracted subgraph. Gets extracting nodes from current subgraph,
    builds pairs of them and for every pair expands subgraph based on that node pair
    in both possible orders of the two nodes.
    """
    extracting_nodes_pairs = itertools.combinations(
		extracting_nodes, 
		2)  
    extracted_subgraph = nx.subgraph(network, extracting_nodes)
    for node1, node2 in extracting_nodes_pairs:
		# look at every pair in both possible directions
		# as our graph is directed
		expand_extracted_subgraph_from_edge(node1, node2, extracted_subgraph, network, max_intermediate_vertices)
		expand_extracted_subgraph_from_edge(node2, node1, extracted_subgraph, network, max_intermediate_vertices)
    return extracted_subgraph

def expand_extracted_subgraph_from_edge(source, target, extracted_subgraph, network, max_intermediate_vertices = MAX_INTERMEDIATE_VERTICES):
    """
    Analyzes the network and decides what edges and nodes
    to add to extracted subgraph based on given source and target nodes.
    The algorithm is: if exists an edge between source and target in network,
    add all nodes and edges on shortest path between souce and target.
    """
    if network.has_edge(source, target):
		shortest_path = nx.shortest_path(
			network,
			source = source, 
			target = target, 
			weight = "weight")
		if(len(shortest_path) - 2 <= max_intermediate_vertices):
			# add all nodes except source and target
			for node in shortest_path:
				if node not in extracted_subgraph:
					extracted_subgraph.add_node(node)
			for i in xrange(len(shortest_path) - 1):
				sp_source = shortest_path[i]
				sp_target = shortest_path[i + 1]
				sp_edge = (
					sp_source,
					sp_target,
					network[sp_source][sp_target]) # edge attributes including weight
				if not extracted_subgraph.has_edge(sp_source, sp_target):
					extracted_subgraph.add_edges_from([sp_edge])


def get_all_extracting_nodes_permutations(text, stimulus, network, ignore_adjectives = True):
	all_permutations = []

	associations = network.neighbors(stimulus)

	if not associations:
		associations = network.neighbors(get_base_form(stimulus))
	if ignore_adjectives:
		associations_in_text = [assoc for assoc in associations if get_base_form(assoc) in text and assoc != stimulus
					and not is_adjective(assoc)]
	else:
		associations_in_text = [assoc for assoc in associations if get_base_form(assoc) in text and assoc != stimulus]

	# Base forms of associations. The dictionary allows to reconstruct the original association.
	base_associations_in_text = {assoc:get_base_form(assoc) for assoc in associations_in_text}

	if len(base_associations_in_text) < 2:
		return []
	
	for associations_permutation in itertools.permutations(base_associations_in_text) :
		all_extracting_nodes = [stimulus] + list(associations_permutation)
		all_permutations.append(all_extracting_nodes)

	return all_permutations

# TODO: Move somewhere else?
class GraphStage:
	"""Represents a stage in extracting subgraph. """
	def __init__(self, stimulus, extracting_nodes, subgraph, is_too_big):
		self.stimulus = stimulus
		self.extracting_nodes = extracting+nodes
		self.subgraph = subgraph
		self.is_too_big = is_too_big