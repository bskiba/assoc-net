# -*- coding: utf-8 -*-

import pylatex
import codecs
from pylatex import Section, Subsection, Document, Figure
import os
import re

#TODO figure out how to merge/differentiate with the file in utils

#TODO: get file names from somehere else (refactoring will be needed)
# maybe move this method elsewhere
def build_tex_for_results(result_path, word):
	notes = []
	word_dir = os.path.join(result_path, "graphs_per_word", word)
	for root, dirs, files in os.walk(word_dir):
		for filename in files:
			match_id = re.match(r"([0-9]+)_note.txt", filename)
			if match_id:
				notes.append(int(match_id.group(1)))
	
	notes = sorted(notes)

	pdf_dir = os.path.join(result_path, "pdf")
	if not os.path.exists(pdf_dir):
		os.makedirs(pdf_dir)
	result_filename = os.path.join(pdf_dir, u"wyniki_{0}".format(word))
	document = Document(result_filename)

	document.append(r"\nonstopmode")
	
	for note_id in notes:
		note_path = os.path.join(word_dir, "{0}_note.txt".format(note_id))
		extracting_nodes_path = os.path.join(word_dir, "{0}_extracting_nodes.txt".format(note_id))
		graph_path = os.path.join(word_dir, "{0}_graph.png".format(note_id))
		print_result_to_tex(document, word, note_id, note_path, graph_path, extracting_nodes_path)

	document.generate_tex()
	document.generate_pdf()

def print_result_to_tex(document, word, note_id, note_path, graph_path, extracting_nodes_path):
	section_name = u"{0} notatka {1}".format(word, note_id)
	with document.create(Section(section_name)):
		note_to_tex(document, note_path)
		extraxting_nodes_to_tex(document, extracting_nodes_path)
		graph_to_tex(document, graph_path)
		document.append(r"\clearpage")

def graph_to_tex(document, graph_path):
	with document.create(Subsection("Graf")):
		with document.create(Figure(position='h!')) as graph_picture:
			graph_picture.add_image(graph_path, width='250px')

def note_to_tex(document, note_path):
	with codecs.open(note_path, encoding='utf-8', mode='r') as note_file:
		with document.create(Subsection("Notatka")):
			document.append(note_file.read())

def extraxting_nodes_to_tex(document, extracting_nodes_path):
	with codecs.open(extracting_nodes_path, encoding='utf-8', mode='r') as extracting_nodes_file:
		with document.create(Subsection("Extracting nodes")):
			document.append(extracting_nodes_file.read())

build_tex_for_results("results/results_2015-04-06_1/", u"praca")
build_tex_for_results("results/results_2015-04-06_1/", u"prawa")
build_tex_for_results("results/results_2015-04-06_1/", u"prawo")
build_tex_for_results("results/results_2015-04-06_1/", u"wieku")
build_tex_for_results("results/results_2015-04-06_1/", u"sad")
